$(document).ready(function(){
    function pergiKe(s){
		$("#loading").css({"visibility" : "visible"});
		var uagent = navigator.userAgent.toLowerCase();

		if(/safari/.test(uagent) && !/chrome/.test(uagent))
		    window.location.href = s;
		else
		    document.location.href = s;
	}
    $(".go-back").click(function(){
        var from = $(this).attr("from");
        pergiKe("../"+from);
    });
    $(".like-button").click(function(){
        if($(this).attr("liked") == "false") {
            $(this).attr("liked", "true");
            $(this).children("img").attr("src", "../ViewTimeline/images/lips_full.png");
        } else {
            $(this).attr("liked", "false");
            $(this).children("img").attr("src", "../ViewTimeline/images/lips_outline.png");
        }
    });
    $(".button-home").click(function() {
        pergiKe("../ViewTimeline/home.html")
    });
    $(".button-notification").click(function() {
        pergiKe("../ViewTimeline/notification.html")
    });
    $(".button-chat").click(function() {
        pergiKe("../ViewChat/home.html")
    });
    $(".category-button").click(function(){
        pergiKe("../ViewShoppink/category.html")
    });
    $(".product-pict-square, .product-pict-long, .product-span, .product-pict-tall").click(function(){
        pergiKe("../ViewShoppink/item.html")
    });
    $(".detail").click(function(){
        pergiKe("../ViewShoppink/item-photos.html")
    });
    $(".add-review").click(function() {
        pergiKe("../ViewShoppink/add-review.html")
    });
    $(".post-review").click(function() {
        pergiKe("../ViewShoppink/item.html")
    });
    $(".emoticon").click(function(){
        //neutralize all
        $(".emoticon").each(function() {
            $(this).attr("selected","false");
            var pict = $(this).attr("def");
            $(this).children("img").attr("src", pict+"_gray.png");
        });

        //activate this
        $(this).attr("selected","true");
        var pict = $(this).attr("def");
        $(this).children("img").attr("src", pict+".png");
    });
    $(".product-pict-thumbnail").click(function(){
        var this_img = $(this).children("img").attr("src");
        $(".product-main").children("img").attr("src", this_img);
        $(".product-pict-thumbnail").each(function() {
            $(this).attr("style", "border-bottom: 0px");
        });
        $(this).attr("style", "border-bottom: 4px solid #dd2267");
    });
    $(".add-to-cart, .buy-now").click(function() {
        pergiKe("../ViewShoppink/basket.html");
    })
    $(".plus").click(function() {
        var amt = $(this).siblings(".amt").html();
        var amt_after = parseInt(amt) + 1;
         $(this).siblings(".amt").html(amt_after);
    }) ;
    $(".minus").click(function() {
        var amt = $(this).siblings(".amt").html();
        var amt_after = parseInt(amt);
        if(amt_after > 1)
            amt_after -= 1;
        $(this).siblings(".amt").html(amt_after);
    }) ;
    $("#check-out").click(function(){
        pergiKe("../ViewShoppink/checkout.html")
    })
    $(".phone").keydown(function(event){
		var data = $(this).val();
		if(((event.which < 48 || event.which > 57) && event.which != 8))
			event.preventDefault();
  	});
    $("#delivery").click(function(){
      pergiKe("../ViewShoppink/delivery.html")
    });
    $("#payment").click(function(){
      pergiKe("../ViewShoppink/payment.html")
    });
});
