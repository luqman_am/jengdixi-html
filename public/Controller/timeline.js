$(document).ready(function(){
    function pergiKe(s){
		$("#loading").css({"visibility" : "visible"});
		var uagent = navigator.userAgent.toLowerCase();

		if(/safari/.test(uagent) && !/chrome/.test(uagent))
		    window.location.href = s;
		else
		    document.location.href = s;
	}
    $(".like-button").click(function(){
        if($(this).attr("liked") == "false") {
            $(this).attr("liked", "true");
            $(this).children("img").attr("src", "../ViewTimeline/images/lips_full.png");
        } else {
            $(this).attr("liked", "false");
            $(this).children("img").attr("src", "../ViewTimeline/images/lips_outline.png");
        }
    });
    $("#menu-expand").click(function(){
        if($(".bottom-menu").attr("expanded") == "false") {
            $(".bottom-menu").attr("expanded", "true");
            $(this).attr("src", "../ViewTimeline/images/bubble_close.png");
            $(".menu-post").slideDown(100);
            $(".menu-delivery").delay(75).slideDown(100);
            $(".menu-shoppink").delay(150).slideDown(100);
            $(".menu-forum").delay(225).slideDown(100);
        } else {
            $(".bottom-menu").attr("expanded", "false");
            $(this).attr("src", "../ViewTimeline/images/bubble_bag.png");
            $(".menu-post").slideUp(100);
            $(".menu-delivery").delay(90).slideUp(100);
            $(".menu-shoppink").delay(180).slideUp(100);
            $(".menu-forum").delay(270).slideUp(100);
        }
    });
    $(".go-profile").click(function(){
        pergiKe("../ViewTimeline/profile.html")
    });
    $(".go-back").click(function(){
        var from = $(this).attr("from");
        pergiKe("../ViewTimeline/"+from);
    });
    $(".identity .name").click(function(){
        var to = $(this).attr("to");
        pergiKe("../ViewTimeline/profile-batur.html");
    })
    $(".notif-row .content .profile").click(function(){
        var to = $(this).attr("to");
        pergiKe("../ViewTimeline/profile-batur.html");
    })
    $(".button-home").click(function() {
        pergiKe("../ViewTimeline/home.html")
    });
    $(".button-notification").click(function() {
        pergiKe("../ViewTimeline/notification.html")
    });
    $(".button-chat").click(function() {
        pergiKe("../ViewChat/home.html")
    });
    $(".menu-shoppink").click(function() {
        pergiKe("../ViewShoppink/home.html")
    });

    // show/hide menu on scroll
    var lastScrollTop = 0;
    $(window).scroll(function() {
        if ($(this).scrollTop()>lastScrollTop) {
            $('.bottom-menu').fadeOut();
        } else {
            $('.bottom-menu').fadeIn();
        }
        lastScrollTop = $(this).scrollTop();
    });
    // end show/hide menu on scroll

    $("#menu-thumbnail").click(function(){
        pergiKe("../ViewTimeline/photos-thumbnail.html");
    });
    $("#menu-timeline").click(function(){
        pergiKe("../ViewTimeline/photos-timeline.html");
    });
    $("#photos").click(function(){
        pergiKe("../ViewTimeline/photos-thumbnail.html");
    });
    $("#friends-other").click(function(){
		pergiKe("../ViewChat/friend-other.html")
	});
	$("#friends").click(function(){
		pergiKe("../ViewChat/home.html")
	});
    $("#settings").click(function(){
        pergiKe("../ViewTimeline/profile-settings.html");
    });
    $("#menu-post").click(function(){
        pergiKe("../ViewTimeline/new-post.html")
    });
    $(".close").click(function() {
        $(this).parent().hide();
        $(".post").show();
        $("#emoticon-list").hide();
    });
    $(".post").click(function() {
        pergiKe("../ViewTimeline/home.html")
    });
    $("#menu-image").click(function(){
        $(".input.attach").hide();
        $(".input.image").show();
        $(".post").show();
        $("#emoticon-list").hide();
    });
    $("#menu-location").click(function(){
        $(".input.attach").hide();
        $(".input.location").show();
        $(".post").show();
        $("#emoticon-list").hide();
    });
    $("#menu-emoticon").click(function(){
        $(".post").hide();
        $("#emoticon-list").show();
    });
    $("#emoticon-list img").click(function(){
        var imgpath = $(this).attr("src");
        $(".input.emoticon img").attr("src",imgpath);
        $(".input.attach").hide();
        $(".input.emoticon").show();
        $(".post").show();
        $("#emoticon-list").hide();
    })
    $("#save-settings").click(function() {
        pergiKe("../ViewTimeline/profile.html");
    });
    $("#menu-forum").click(function() {
        pergiKe("../ViewForum/home.html");
    });
    $("#activity").click(function() {
        pergiKe("../ViewWizard/activity.html");
    });
    $(".add-comment").click(function() {
        pergiKe("../ViewTimeline/add-comment.html");
    });
    $(".search").click(function() {
      pergiKe("../ViewTimeline/search.html")
    });
});
