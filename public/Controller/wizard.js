$(document).ready(function(){
    function pergiKe(s){
		$("#loading").css({"visibility" : "visible"});
		var uagent = navigator.userAgent.toLowerCase();

		if(/safari/.test(uagent) && !/chrome/.test(uagent))
		    window.location.href = s;
		else
		    document.location.href = s;
	}
    $(".go-back").click(function(){
        var from = $(this).attr("from");
        pergiKe("../"+from);
    });
    $(".like-button").click(function(){
        if($(this).attr("liked") == "false") {
            $(this).attr("liked", "true");
            $(this).children("img").attr("src", "../ViewTimeline/images/lips_full.png");
        } else {
            $(this).attr("liked", "false");
            $(this).children("img").attr("src", "../ViewTimeline/images/lips_outline.png");
        }
    });
    $(".button-home").click(function() {
        pergiKe("../ViewTimeline/home.html")
    });
    $(".button-notification").click(function() {
        pergiKe("../ViewTimeline/notification.html")
    });
    $(".button-chat").click(function() {
        pergiKe("../ViewChat/home.html")
    });
    $("#hot-stuffs").click(function() {
        pergiKe("../ViewWizard/hot-stuffs.html")
    });
    $("#wizard").click(function() {
        pergiKe("../ViewWizard/wizard-home.html")
    });
    $(".bar-overview-link").click(function() {
        pergiKe("../ViewWizard/smart-saving.html")
    });
    $("#shopping-overview-button").click(function(){
      pergiKe("../ViewWizard/shopping-overview.html")
    });
});
