$(document).ready(function(){
	function updateScroll() {
		$(document).scrollTop($('#chat-body').prop("scrollHeight"));
		//alert($('#chat-body').prop("scrollHeight") + " " +$(document).scrollTop());
	}
	function hideAllMenu() {
		$(".bar-white").slideUp();
		$("#cover").fadeOut();
		$(".bar-bottom .menu").slideUp();
		$(".notification").fadeOut();
		$("#cover").attr("active","false");
		$(".bar-white").attr("active","false");
		$(".bar-bottom .menu").attr("active","false");
		$(".notification").attr("active","false");
	}
	function showNotification(text) {
		$("#notif-text").html(text);
		$(".notification").fadeIn();
		$(".notification").attr("active","true");
		$("#cover").fadeIn();
		$("#cover").attr("active","true");
	}
	function showDefaultNotification() {
		$(".notification").fadeIn();
		$(".notification").attr("active","true");
		$("#cover").fadeIn();
		$("#cover").attr("active","true");
	}
	updateScroll();
    function pergiKe(s){
		$("#loading").css({"visibility" : "visible"});
		var uagent = navigator.userAgent.toLowerCase();

		if(/safari/.test(uagent) && !/chrome/.test(uagent))
		    window.location.href = s;
		else
		    document.location.href = s;
	}
    $(".go-back").click(function(){
        var from = $(this).attr("from");
        pergiKe("../"+from);
    });
	$(".networking-content-item .title").click(function(){
		var active = $(this).attr("active");
		if(active == "true") {
			$(this).attr("active", "false");
			$(this).children(".groups-title").removeClass("active");
			$(this).children("img").attr("src", "images/up_gray.png");
			$(this).siblings(".list").slideUp();
		} else {
			$(this).attr("active", "true");
			$(this).children(".groups-title").addClass("active");
			$(this).children("img").attr("src", "images/down.png");
			$(this).siblings(".list").slideDown();
		}
	});
	$(".card .header").click(function(){
		var type = $(this).attr("type");
		if(type == "group") {
			pergiKe("../ViewChat/chat-group.html");
		} else {
			pergiKe("../ViewChat/chat-one.html");
		}
	});
	$("#menu-chat-toggle").click(function(){
		var isTopActive = $(".bar-white").attr("active");
		var isBottomActive = $(".bar-bottom .menu").attr("active");
		if(isBottomActive == "true") {
			$(".bar-white").slideToggle();
			if(isTopActive == "true") {
				$(".bar-white").attr("active", "false");
			} else {
				$(".bar-white").attr("active", "true");
			}
		} else {
			if(isTopActive == "true") {
				hideAllMenu();
			} else {
				$(".bar-white").slideToggle();
				$(".bar-white").attr("active", "true");
				$("#cover").fadeToggle();
				$("#cover").attr("active", "true");
			}
		}
	});
	$("#menu-attach-main").click(function(){
		var isTopActive = $(".bar-white").attr("active");
		var isBottomActive = $(".bar-bottom .menu").attr("active");
		if(isTopActive == "true") {
			$(".bar-bottom .menu").slideToggle();
			if(isBottomActive == "true") {
				$(".bar-bottom .menu").attr("active", "false");
			} else {
				$(".bar-bottom .menu").attr("active", "true");
			}
		} else {
			if(isBottomActive == "true") {
				hideAllMenu();
			} else {
				$(".bar-bottom .menu").slideToggle();
				$(".bar-bottom .menu").attr("active", "true");
				$("#cover").fadeToggle();
				$("#cover").attr("active", "true");
			}
		}
	});
	$("#menu-emoticon").click(function(){
		//alert("duer");
	});
	$("#cover").click(function(){
		hideAllMenu();
	});
	$("#menu-send").click(function(){
		var content = $("#chat-text").val();
		$("#chat-text").val("");
		var text = '<div class="chat-item self">' +
            '<div class="time">' +
                '<span class="read">Read </span><br />' +
                '08:00' +
            '</div>' +
            '<div class="msg">' +
                content +
            '</div>'+
        '</div>'+
        '<div style="clear:both;"></div>';
		if(content != "") {
			$(".read").hide();
			$(".chat-body").append(text);
		}
		updateScroll();
	});
	$("#menu-left-group").click(function(){
		$(".bar-white").slideUp();
		$(".bar-white").attr("active", "false");
		$(".bar-bottom .menu").slideUp();
		$(".bar-bottom .menu").attr("active", "false");
		showNotification("Are you sure that you want to leave this group?");
	});
	$("#button-no").click(function(){
		hideAllMenu();
	});
	$("#button-yes").click(function(){
		hideAllMenu();
	});
	$("#menu-edit-group").click(function(){
		pergiKe("../ViewChat/edit-group.html");
	});
	$(".group-title .button, .group-title .group-button").click(function(){
		if($(".group-title").attr("editing") == "false") {
			// show input, hide text
			$(".group-title .text").hide();
			$(".group-title .input").val($(".group-title .text").html());
			$(".group-title .input").show();
			$(this).attr("src", "images/finish_edit_group.png");
			$(".group-title").attr("editing", "true")
		} else {
			$(".group-title .input").hide();
			$(".group-title .text").html($(".group-title .input").val());
			$(".group-title .text").show();
			$(this).attr("src", "images/edit_group.png");
			$(".group-title").attr("editing", "false")
		}
	});
	$(".remove").click(function(){
		var name = $(this).siblings(".name").html();
		showNotification("Do you want to remove "+name+" from group?");
	});
	$("#menu-block").click(function(){
		var blocked = $(this).attr("blocked");
		var type = $(this).attr("type");
		if(blocked == "false"){
			var message =
			'<div class="chat-item join">'+
                '<div class="msg">'+
                    'This '+type+' has been blocked'
                '</div>'+
            '</div>'+
            '<div style="clear:both;"></div>';
			$(this).attr("blocked", "true");
			$(this).siblings(".text").html("Unblock");
		} else {
			var message =
			'<div class="chat-item join">'+
                '<div class="msg">'+
                    'This '+type+' has been unblocked'
                '</div>'+
            '</div>'+
            '<div style="clear:both;"></div>';
			$(this).attr("blocked", "false");
			$(this).siblings(".text").html("Block");
		}

		if(message != "") {
			$(".chat-body").append(message);
		}
		hideAllMenu();
		updateScroll();
	});

	$("#menu-invite-to").click(function(){
		showDefaultNotification();
	});

	$("#menu-media").click(function(){
		pergiKe("../ViewChat/album.html")
	});
	$("#menu-gallery").click(function(){
		pergiKe("../ViewChat/album.html")
	});
	$("#menu-contact").click(function(){
		pergiKe("../ViewChat/select-friend.html")
	});
	$("#menu-location").click(function(){
		pergiKe("../ViewChat/select-location.html")
	});
	$("#button-home").click(function() {
        pergiKe("../ViewTimeline/home.html")
    });
    $("#button-notification").click(function() {
        pergiKe("../ViewTimeline/notification.html")
    });
    $("#button-chat").click(function() {
        pergiKe("../ViewChat/home.html")
    });
	$(".photos .image-container").click(function(){
		pergiKe("../ViewChat/chat-one.html")
	});
	$("#button-select").click(function(){
		pergiKe("../ViewChat/chat-one.html");
	});
});
