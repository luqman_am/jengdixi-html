var lagiOn = false;

$(document).ready(function(){

	var globalSocket = io.connect('http://jengdixi.eu-1.evennode.com/');
	var kodeApp = 1;
	var canlogin = false;

	function blur(hidup){
		if(hidup){
			$("#mainInline").css({"filter" : "blur(3px)"});
			$("#mainInline").css({"-webkit-filter" : "blur(3px)"});
			$("#mainInline").css({"-moz-filter" : "blur(3px)"});
			$("#mainInline").css({"-o-filter" : "blur(3px)"});
			$("#mainInline").css({"-ms-filter" : "blur(3px)"});
		}else{
			$("#mainInline").css({"filter" : "blur(0px)"});
			$("#mainInline").css({"-webkit-filter" : "blur(0px)"});
			$("#mainInline").css({"-moz-filter" : "blur(0px)"});
			$("#mainInline").css({"-o-filter" : "blur(0px)"});
			$("#mainInline").css({"-ms-filter" : "blur(0px)"});
		}
	}

	function blurLogin(hidup){
		if(hidup){
			$("#mainInline").css({"filter" : "blur(3px)"});
			$("#mainInline").css({"-webkit-filter" : "blur(3px)"});
			$("#mainInline").css({"-moz-filter" : "blur(3px)"});
			$("#mainInline").css({"-o-filter" : "blur(3px)"});
			$("#mainInline").css({"-ms-filter" : "blur(3px)"});
		}else{
			$("#mainInline").css({"filter" : "blur(0px)"});
			$("#mainInline").css({"-webkit-filter" : "blur(0px)"});
			$("#mainInline").css({"-moz-filter" : "blur(0px)"});
			$("#mainInline").css({"-o-filter" : "blur(0px)"});
			$("#mainInline").css({"-ms-filter" : "blur(0px)"});
		}
	}

	function show(){
		if($("#popupRegistrasi").css("visibility") == "visible" && !lagiOn){
			$("#popupRegistrasi").css({"opacity" : "0", "visibility" : "hidden"});
			blur(false);
		}else{
			lagiOn = false;
			$("#popupRegistrasi").css({"visibility" : "visible", "opacity" : "1"});
			blur(true);
			lagiOn = true;
		}
	}

	function showForget(){
		if($("#popup-forget").css("visibility") == "visible" && !lagiOn){
			$("#popup-forget").css({"opacity" : "0", "visibility" : "hidden"});
			blurLogin(false);
		}else{
			lagiOn = false;
			$("#popup-forget").css({"visibility" : "visible", "opacity" : "1"});
			blurLogin(true);
			lagiOn = true;
		}
	}


	function affect(){
		if($("#popupRegistrasi").css("visibility") == "visible" && !lagiOn){
			$("#popupRegistrasi").css({"opacity" : "0", "visibility" : "hidden"});
			blur(false);
		}else{
			lagiOn = false;
		}
	}

	function affectForget(){
		if($("#popup-forget").css("visibility") == "visible" && !lagiOn){
			$("#popup-forget").css({"opacity" : "0", "visibility" : "hidden"});
			blurLogin(false);
		}else{
			lagiOn = false;
		}
	}

	function olahNoHP2(data){
		/*if(data.length < 3 && data != "+6"){
			$("#dataRegisterT1NoHP").val("+62" + data);
		}else if(data == "+6"){
			$("#dataRegisterT1NoHP").val("+62");
		}*/
		cekData();
	}

	function cekData(){
		var inpEmail = $("#dataRegisterT1Email").val();
		var inpPassword = $("#dataRegisterT1Password").val();
		var inpRetypePassword = $("#dataRegisterT1RePassword").val();
		var inpNoHP = $("#dataRegisterT1NoHP").val();

		if(inpEmail.length > 2 && inpPassword.length > 4 && inpNoHP.length > 3 && (inpPassword == inpRetypePassword)){
			$("#dataOK").html("Ok!").css({"color" : "#dd2267", "font-size" : "4vw"}).attr('onclick', 'pergiKe("verifikasi.html")');
		}else{

			$("#dataOK").html("").css({"color" : "#BBB", "font-size" : "4vw"}).attr('onclick', '');
		}
	}

	function pergiKe(s){
		$("#loading").css({"visibility" : "visible"});
		var uagent = navigator.userAgent.toLowerCase();
		
		if(/safari/.test(uagent) && !/chrome/.test(uagent))
		    window.location.href = s;
		else
		    document.location.href = s;
	}

	function keDaftar(){
		if($("#dataOK").html() == "Ok!"){
			pergiKe("registrasi.html");
		}
	}

	window.keDaftarLengkap = function(){
		var delay = 2000; //1 second

		setTimeout(function() {
			pergiKe("registrasi.html");
		}, delay);
	};

	window.keLoginDariVerifikasi = function(){
		var delay = 2500; //2.5 second

		setTimeout(function() {
			pergiKe("login.html");
		}, delay);
	};

	function keSelesaiDaftar(){
		$("#loading").css({"visibility" : "visible"});
		pergiKe("title.html");
	}

	function pilihJK(cowo){
		if(cowo){
			$("#cowo").addClass("jkselected");
			$("#cewe").removeClass("jkselected");
			$("#cowo").attr("src", "image/Registras_07.png");
			$("#cewe").attr("src", "image/Registrasi_25.png");
		}else{
			$("#cewe").addClass("jkselected");
			$("#cowo").removeClass("jkselected");
			$("#cowo").attr("src", "image/Registrasi_22.png");
			$("#cewe").attr("src", "image/Registras_10.png");
		}
		cekRegistrasi2();
	}

	function validUsernamePasswordLogin(un, pw){
		return true;
	}

	function validEmailPasswordNoHPRegisterT1(em, pw, nh){
		return true;
	}

	function validUsernameKodeVerifikasiT21(un, kv){
		return true;
	}

	// KIRIM <LOGIN>
	function kirimDataLogin(){
		var un = $("#dataLoginUsername").val();
		var pw = $("#dataLoginPassword").val();

		if(validUsernamePasswordLogin(un, pw)){
			var data = "";
			globalSocket.emit('login', data);
		}else{
			alert("username atau password tidak valid");
		}
	}

	// RESPON <LOGIN>
	globalSocket.on('login', function(msg){
		if(msg == "1")
			alert("berhasil");
		else
			alert("gagal");
	});

	// KIRIM <DAFTAR TAHAP 1>
	function kirimDataRegisterTahap1(){
		var em = $("#dataRegisterT1Email").val();
		var pw = $("#dataRegisterT1Password").val();
		var nh = $("#dataRegisterT1NoHP").val();

		if(validEmailPasswordNoHPRegisterT1(em, pw, nh)){
			var data = {kode_app: kodeApp, email: em, password: pw, nohp : nh};
			globalSocket.emit('Register1', data);
		}else{
			alert("email, password, atau nohp salah");
		}
	}

	// RESPON <DAFTAR TAHAP 1>
	globalSocket.on('Register1', function(msg){
		switch(msg){
			case "1" : 
				alert("tahap 1 berhasil");
				pergiKe("verifikasi.html");
				break;

			case "2" :
				alert("email sudah digunakan sebelumnya");
				break;

			case "3" :
				alert("nohp sudah digunakan sebelumnya");
				break;

			default: 
				alert("kesalahan: tidak dikenali");
		}
	});



	// KIRIM <DAFTAR TAHAP 21>
	function kirimDataRegisterTahap21(){
		var un = $("#dataRegisterT2FirstName").val();
		var kv = $("#dataRegisterT2LastName").val();

		if(validUsernameKodeVerifikasiT21(un, kv)){
			var data = {username: un, kode: kv};
			globalSocket.emit('Register2', data);
		}else{
			alert("username atau kode verifikasi salah");
		}
	}

	// RESPON <DAFTAR TAHAP 21>
	globalSocket.on('Register2', function(msg){
		if(msg == "0"){
			alert("kode verifikasi salah");
		}else{
			alert("berhasil");
			kirimDataRegisterTahap22();
		}
	});

	// KIRIM <DAFTAR TAHAP 22>
	function kirimDataRegisterTahap22(){
		var un = $("#dataRegisterT2FirstName").val();
		var kv = $("#dataRegisterT2LastName").val();
		var jk = $("#cowo").attr("src") == "image/Registras_07.png" ? "1" : "0";

		var data = {username: un, kode: kv, jk: jk};
		globalSocket.emit('Register3', data);
	}

	function cekRegistrasi2() {
		var firstName = $("#dataRegisterT2FirstName").val();
		var kelamin = $("#cowo").hasClass("jkselected") || $("#cewe").hasClass("jkselected");

		if(firstName != "" && kelamin) {
			$("#signMe").html("Sign me in!");
			$("#signMe").addClass("cansignin");
		} else {
			$("#signMe").html("");
			$("#signMe").removeClass("cansignin");
		}
	}

	// RESPON <DAFTAR TAHAP 22>
	globalSocket.on('Register3', function(msg){
		alert("berhasil, respon server : " + msg);
	});

	$(".tblSignup").click(function(){
		show();
	});

	$(".affect").click(function(){
		affect();
	});

	$(".affect-login").click(function(){
		affectForget();
	});

	$(".kelogin").click(function(){
		pergiKe("login.html");
	});

	$(".keindex").click(function(){
		pergiKe("title.html");
	});

	$(".registrasiPilihJK0").click(function(){
		pilihJK(false);
	});

	$(".registrasiPilihJK1").click(function(){
		pilihJK(true);
	});

	$(".kedaftar").click(function(){
		keDaftar();
		//kirimDataRegisterTahap1();
	});

	$(".selesaiDaftar").click(function(){
		if($("#signMe").hasClass("cansignin")) {
			keSelesaiDaftar();
		}
	});

	$(".veriLogin").click(function(){
		kirimDataLogin();
	});

	$(".indexCekData").keyup(function(){
		cekData();
	});

	$(".indexOlahNoHP").keydown(function(event){
		var data = $(this).val();
		if(((event.which < 48 || event.which > 57) && event.which != 8))
			event.preventDefault();
		else
			olahNoHP2(data);
	});
	$(".intro-close").click(function(){
		pergiKe("title.html");
	});

	$("#dataRegisterT2FirstName").keyup(function(event){
		cekRegistrasi2();
	});
	$("#forget-password").click(function(){
		//pergiKe("forget.html");
		showForget();
	});
	$(".forget-send-button").click(function(event){
		var email = $("#data-email").val();
		if(email != "") {
			pergiKe("verifikasi.html");
		}
	});
	$("#lihat-password").mousedown(function(event){
		$(this).attr("src", "image/mata_active_fix.png");
		$("#dataLoginPassword").attr("type", "text");
	});
	$("#lihat-password").mouseup(function(event){
		$(this).attr("src", "image/mata_inactive_fix.png");
		$("#dataLoginPassword").attr("type", "password");
	});
	$("#login-hapus").click(function(event){
		$("#dataLoginUsername").val("");
	});

	$("#dataLoginPassword").keyup(function(){
		cekDataLogin();
	});
	$("#dataLoginUsername").keyup(function(){
		cekDataLogin();
	});

	function cekDataLogin() {
		var email = $("#dataLoginUsername").val();
		var password = $("#dataLoginPassword").val();
		if(email.length > 0 && password.length > 0) {
			canlogin = true;
			$("#loginbutton").addClass("can-login");
		} else {
			canlogin = false;
			$("#loginbutton").removeClass("can-login");
		}
	}
	$("#loginbutton").click(function(){
		if(canlogin) {
			pergiKe("../ViewTimeline/home.html");
		}
	});
})
